<?php
	header("Cache-Control: no-cache, must-revalidate");
	
	session_destroy();
	session_start();
	session_unset(); 
	
	$startdir = 'files';
	
	require("functions.php");
	setupDirectory('$startdir');
	header("Location: get_directory.php?path=$startdir"); 
?>